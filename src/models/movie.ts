import { posterPathUrl } from "../config";

export class Movie {
    public id: number;
    public title: string;
    public poster: string;
    public date: Date;
    public popularity: number;
    public voteCount: number;
    public overview: string;

    
    constructor({ id, poster_path, title, release_date, popularity, vote_count, overview }: any) {
        this.id = id;
        this.title = title;
        this.poster = posterPathUrl+'/'+poster_path;
        this.date = release_date;
        this.popularity = popularity;
        this.voteCount = vote_count;
        this.overview = overview;
    }
}