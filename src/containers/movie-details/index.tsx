import React from "react";
import {
  Container,
  Poster,
  Title,
  Description,
  Vote,
  Popularity,
  ReleaseDate,
} from "./styles";
import { withRouter } from "react-router-dom";
import { useMovieDetailsFetch } from "./hooks";
import { Loader } from "../../component-ui/loader";

const MovieDetailsContainer = withRouter(({ match }: any) => {

  // HOOKS  
  const { movie, isLoading } = useMovieDetailsFetch(match.params.id);

  // RENDERS
  return (
    <Container>
      {isLoading ? (
        <Loader />
      ) : (
        <>
          <Poster posterPath={movie.poster} />
          <Title>{movie.title}</Title>
          <ReleaseDate>Release date: {movie.date}</ReleaseDate>
          <Vote>
            <p>Vote count: {movie.voteCount}</p>
          </Vote>
          <Popularity>
            <p>Popularity: {movie.popularity}</p>
          </Popularity>
          <Description>{movie.overview}</Description>
        </>
      )}
    </Container>
  );
});

export { MovieDetailsContainer };
