import { useDispatch, useSelector } from "react-redux";
import { fetchMovies } from "./action";
import { useEffect } from "react";
import { selectMovies, selectLoadingStatus } from "../../selectors";

export const useMoviesFetch = () => {
    const dispatch = useDispatch();
    function ComponentDidMount() {
      dispatch(fetchMovies());
    }
    useEffect(ComponentDidMount, []);

    const movies = useSelector(selectMovies);
    const isLoading = useSelector(selectLoadingStatus);

    return {movies, isLoading};
  };