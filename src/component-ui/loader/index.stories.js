import React from "react";
import { Loader } from ".";

export default {
  title: "Loader",
  component: Loader
};

const rootStyle = {width: '14rem'};

export const LoaderStand = () => (
    <div style={rootStyle}>
      <Loader />
    </div>
);
