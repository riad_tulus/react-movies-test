import React from "react";
import { render, fireEvent } from "@testing-library/react";
import { Card } from "./index";
import { Movie } from "../../models/movie";

const img_path = "/xBHvZcjRiWyobQ9kxBhO6B2dtRI.jpg";

const movieMock = new Movie({
  id: 1,
  poster_path: img_path,
  title: "casa de papel",
  popularity: 100.0,
  vote_count: 100,
  release_date: "12/12/2019",
});

test("should match snapshot and render correctly", () => {
  const { container } = render(
    <Card movie={movieMock} key={1} onClick={() => {}} />
  );
  expect(container).toMatchSnapshot();
});

test("Card must contain the expected movie data", () => {
  const { getByText } = render(
    <Card movie={movieMock} key={1} onClick={() => {}} />
  );
  const title = getByText("casa de papel");
  expect(title.innerHTML).toBe("casa de papel");
});

test("Card should handle the click event", () => {
  const onClickEventMock = jest.fn();
  const { getByText } = render(
    <Card movie={movieMock} key={1} onClick={onClickEventMock} />
  );
  const title = getByText("casa de papel");
  fireEvent.click(title);
  expect(onClickEventMock).toHaveBeenCalled();
});
